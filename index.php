<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $object = new animal("shaun");

    echo "Name : ". $object ->name . "<br>";
    echo "legs : ". $object ->legs . "<br>";
    echo "cold blooded : ". $object ->cold_blooded . "<br><br>";

    $object2 = new frog("buduk");

    echo "Name : $object2->name <br>";
    echo "legs : $object2->legs <br>";
    echo "cold blooded : $object2->cold_blooded <br>";
    $object2->hophop();

    $object3 = new ape("kera sakti");

    echo "Name : $object3->name <br>";
    echo "legs : $object3->legs <br>";
    echo "cold blooded : $object3->cold_blooded <br>";
    $object3->yell();
?>